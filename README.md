Sito personale, template clonato da [furkanpehli1](https://github.com/rimijoker/Fake_Terminal_Resume_Website) al quale ho effettuato qualche modifica come ad esempio i colori, permesso anche di essere visto da chi ha JavaScript disabilitato ed eliminata la connessione a Google utilizzando in locale jQuery.

Il font invece è stato cambiato e quello utilizzato ora è [Hack](https://github.com/chrissimpkins/Hack)

Il sito viene generato automaticamente su: [guani.it](guani.it)